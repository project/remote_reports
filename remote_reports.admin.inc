<?php

/**
 * @file
 * Remote Reports Server module administration settings.
 */

/**
 * Inits remote_reports_conf.
 */
function remote_reports_conf() {
  $build = array();

  // Array to contain rows for table render.
  $rows = array();

  // Table header.
  $header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'sites.title',
    ),
    'hostname' => array(
      'data' => t('Hostname'),
      'field' => 'sites.hostname',
    ),
    'url' => array(
      'data' => t('Site URL'),
    ),
    'api_key' => array(
      'data' => t('API key'),
    ),
    'actions' => array(
      'data' => t('Actions'),
    ),
  );

  // Use our custom function to retrieve data.
  $sites = _get_sites($header);

  // Iterate over the result set and format as links.
  foreach ($sites as $site) {
    $rows[] = array(
      'data' => array(
        'title' => $site->title,
        'hostname' => $site->hostname,
        'url' => $site->url,
        'api_key' => $site->api_key,
        'actions' => l(t('Edit'), 'admin/config/system/remote_reports/edit/' . $site->sid) . " " . l(t('Delete'), 'admin/config/system/remote_reports/delete/' . $site->sid),
      ),
    );
  }

  // No content found.
  if (empty($rows)) {
    $build['content']['#markup'] = '<p>' . t('No sites found. Click on "Add a site" to add one.') . '</p>';
  }
  else {
    // Pass data through theme function.
    $build['content'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No sites found. Click on "Add a site" to add one.'),
    );
  }

  return $build;
}

/**
 * Form to add a site.
 */
function remote_reports_add_site_form($form, &$form_state) {
  $api_key = drupal_random_key(23);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title to remember what this key is for.'),
    '#required' => TRUE,
  );

  $form['hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#description' => t('Hostname to group sites together'),
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Site URL'),
    '#length' => 255,
    '#description' => t('The URL of the site'),
    '#required' => TRUE,
  );

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('The API key that will be used.'),
    '#default_value' => $api_key,
    '#disabled' => TRUE,
    '#required' => TRUE,
  );

  // When this button is pressed the form reloads
  // causing a new api key to be generated.
  $form['generate_api_key'] = array(
    '#type' => 'button',
    '#value' => t('Generate new API key'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save site'),
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function remote_reports_add_site_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('The url must be valid'));
  }
  if (isset($form_state['values']['hostname'])) {
    $hostname_regex = '/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/';
    if (!preg_match($hostname_regex, $form_state['values']['hostname'])) {
      form_set_error('hostname', t('The hostname must be valid or empty'));
    }
  }
}

/**
 * Implements hook_submit().
 */
function remote_reports_add_site_form_submit($form, &$form_state) {
  // Insert the new site.
  $sid = db_insert('remote_reports_sites')
    ->fields(array(
      'hostname' => $form_state['values']['hostname'],
      'title' => $form_state['values']['title'],
      'url' => $form_state['values']['url'],
      'api_key' => $form_state['values']['api_key'],
    ))
    ->execute();

  // Display a success message.
  drupal_set_message(t('The site %site has been added.', array('%site' => $form_state['values']['title'])), 'status', TRUE);
  // Redirect to the default config page.
  $form_state['redirect'] = url('admin/config/system/remote_reports');
}

/**
 * Form to edit a site.
 */
function remote_reports_edit_site_form($form, &$form_state, $sid) {
  $site = _get_site($sid);

  $form['site_id'] = array('#type' => 'value', '#value' => $sid);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title to remember what this key is for.'),
    '#required' => TRUE,
    '#default_value' => $site->title,
  );

  $form['hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#description' => t('Hostname to group sites together'),
    '#default_value' => $site->hostname,
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Site URL'),
    '#description' => t('The URL of the site'),
    '#required' => TRUE,
    '#default_value' => $site->url,
  );

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('The API key that will be used.'),
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#default_value' => (isset($form_state['storage']['new_key']) ? $form_state['storage']['new_key'] : $site->api_key),
  );

  $form['generate_api_key'] = array(
    '#type' => 'submit',
    '#value' => t('Generate new API key'),
    '#name' => 'generate_new_key',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save site'),
    '#name' => 'submit',
  );
  return $form;
}

/**
 * Implements hook_validate().
 */
function remote_reports_edit_site_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('The url must be valid'));
  }
  if (isset($form_state['values']['hostname'])) {
    $hostname_regex = '/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/';
    if (!preg_match($hostname_regex, $form_state['values']['hostname'])) {
      form_set_error('hostname', t('The hostname must be valid or empty'));
    }
  }
}

/**
 * Implements hook_submit().
 */
function remote_reports_edit_site_form_submit(&$form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'submit') {
    // Update the edited site.
    db_update('remote_reports_sites')
      ->fields(array(
        'hostname' => $form_state['values']['hostname'],
        'title' => $form_state['values']['title'],
        'url' => $form_state['values']['url'],
        'api_key' => $form_state['values']['api_key'],
      ))
      ->condition('sid', $form_state['values']['site_id'], '=')
      ->execute();

    // Display a success message.
    drupal_set_message(t('The site %site has been edited.', array('%site' => $form_state['values']['title'])), 'status', TRUE);
    // Redirect to the default config page.
    $form_state['redirect'] = url('admin/config/system/remote_reports');
  }
  else {
    if ($form_state['clicked_button']['#name'] == 'generate_new_key') {
      $api_key = drupal_random_key(23);

      $form_state['rebuild'] = TRUE;

      $form_state['storage']['new_key'] = $api_key;
    }
  }
}

/**
 * Form to delete a site.
 */
function remote_reports_delete_site_confirm($form, &$form_state, $sid) {
  $site = _get_site($sid);
  $form['site_id'] = array('#type' => 'value', '#value' => $sid);
  $form['title'] = array('#type' => 'value', '#value' => $site->title);
  return confirm_form($form,
    t('Confirm deletion'),
    'admin/config/system/remote_reports',
    t('Are you sure you want to delete the site: %title?', array('%title' => $site->title)),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implements hook_submit().
 */
function remote_reports_delete_site_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    // Delete from sites.
    db_delete('remote_reports_sites')
      ->condition('sid', $form_state['values']['site_id'])
      ->execute();
    // Delete from caches.
    db_delete('cache_remote_reports_status')
      ->condition('sid', $form_state['values']['site_id'])
      ->execute();
    db_delete('cache_remote_reports_site_modules')
      ->condition('sid', $form_state['values']['site_id'])
      ->execute();
    db_delete('cache_remote_reports_site_status')
      ->condition('sid', $form_state['values']['site_id'])
      ->execute();

    // Display a success message.
    drupal_set_message(t('The site %site has been deleted.', array('%site' => $form_state['values']['title'])), 'status', TRUE);
    // Redirect to the default config page.
    $form_state['redirect'] = url('admin/config/system/remote_reports');
  }
}
