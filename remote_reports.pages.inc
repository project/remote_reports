<?php

/**
 * @file
 * Remote Reports Server module administration settings.
 */

/**
 * Form function that contains all filter forms for the dashboard page.
 */
function remote_reports_filter_form($form, &$form_state) {
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'tab1',
  );
  // Form to search site by module.
  $form['tab1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search by module'),
    '#group' => 'vertical_tabs',
  );
  $form['tab1']['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#value' => !empty($_GET['module']) ? $_GET['module'] : '',
    '#name' => 'module',
    '#autocomplete_path' => 'admin/remote_reports/autocomplete/modules',
  );
  // Get versions if a module is searched.
  if (!empty($_GET['module'])) {
    $module_versions = array(
      NULL => t('Choose a version...'),
    );
    // Get the site modules that match.
    $modules = _cache_site_modules_get(NULL, $_GET['module']);
    foreach ($modules as $module) {
      // Put all the module versions in an array.
      $module_versions[$module['info']['version']] = $module['info']['version'];
    }
  }
  else {
    $module_versions = array(
      NULL => t('First search a module'),
    );
  }
  $form['tab1']['module_status'] = array(
    '#type' => 'radios',
    '#title' => 'Status',
    '#options' => array(
      2 => 'Both',
      1 => 'Enabled',
      0 => 'Disabled',
    ),
    '#default_value' => isset($_GET['module_status']) ? $_GET['module_status'] : 2,
  );
  $form['tab1']['module_version'] = array(
    '#type' => 'select',
    '#title' => t('Version'),
    '#options' => $module_versions,
    '#value' => !empty($_GET['module_version']) ? $_GET['module_version'] : '',
    '#disabled' => empty($_GET['module']) ? TRUE : FALSE,
    '#name' => 'module_version',
  );

  $form['tab1']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

/**
 * Inits dashboard page.
 */
function remote_reports_page_dashboard() {
  $build = array();
  // Array to contain rows for table render.
  $rows = array();
  // Table header.
  $header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'sites.title',
    ),
    'hostname' => array(
      'data' => t('Hostname'),
      'field' => 'sites.hostname',
    ),
    'errors' => array(
      'data' => t('Errors'),
      'field' => 'status.errors',
    ),
    'warnings' => array(
      'data' => t('Warnings'),
      'field' => 'status.warnings',
    ),
    'checks' => array(
      'data' => t('Checks'),
      'field' => 'status.checks',
    ),
    'not_secure' => array(
      'data' => t('Security update'),
      'field' => 'status.not_secure',
    ),
    'not_supported' => array(
      'data' => t('Not supported'),
      'field' => 'status.not_supported',
    ),
    'not_current' => array(
      'data' => t('Available updates'),
      'field' => 'status.not_current',
    ),
    'status_created' => array(
      'data' => t('Last status refresh'),
      'field' => 'status.status_created',
    ),
    'modules_created' => array(
      'data' => t('Last modules refresh'),
      'field' => 'status.modules_created',
    ),
  );
  // TODO: Fix table sorting after search.
  // Check if the filter by module is set.
  if (!empty($_GET['module'])) {
    // Get the sid from sites that use the module.
    $modules = _cache_site_modules_get(NULL, $_GET['module']);
    // Get site status summary for every site that uses the module.
    $sites = array();
    foreach ($modules as $module) {
      if ($_GET['module_status'] == $module['project_status'] || $_GET['module_status'] == 2) {
        // Only get site if the version matches.
        if (!empty($_GET['module_version'])) {
          if ($_GET['module_version'] == $module['info']['version']) {
            $sites[] = _get_site_status($module['sid'], $header);
          }
        }
        else {
          $sites[] = _get_site_status($module['sid'], $header);
        }
      }
    }
  }
  else {
    // Get the site status summary.
    $sites = _get_site_status(NULL, $header);
  }
  foreach ($sites as $site) {
    // First check if the site has loaded the status report.
    if (!isset($site->status_created)) {
      drupal_set_message(t('The site %site has no data. <a href="@refresh">Refresh it</a>.',
        array(
          '@refresh' => '/admin/remote_reports/refresh/site/' . $site->sid . '?destination=' . drupal_get_destination()['destination'],
          '%site' => $site->title,
        )), 'error');
    }
    // Proceed if no errors occurred.
    else {
      // Change color of row depending on errors and updates.
      if ($site->errors > 0
        || $site->not_secure > 0
      ) {
        $class = 'error';
      }
      else {
        if ($site->warnings > 0
          || $site->not_supported > 0
          || $site->not_current > 0
        ) {
          $class = 'warning';
        }
        else {
          $class = 'ok';
        }
      }
      $rows[] = array(
        'data' => array(
          'title' => l($site->title, 'admin/remote_reports/site/' . $site->sid),
          'hostname' => $site->hostname,
          'error' => $site->errors,
          'warnings' => $site->warnings,
          'checks' => $site->checks,
          'not_secure' => $site->not_secure,
          'not_supported' => $site->not_supported,
          'not_current' => $site->not_current,
          'status_created' => date('H:i d/m/Y', $site->status_created),
          'modules_created' => date('H:i d/m/Y', $site->modules_created),
        ),
        'class' => array($class),
      );
    }
  }

  // Render the filter form.
  $filter_form = drupal_get_form('remote_reports_filter_form');
  $build['filter'] = array(
    '#theme' => 'form',
    '#children' => drupal_render($filter_form),
  );
  // TODO: No site with module found.
  // Check if there are sites.
  if (empty($rows)) {
    $build['sites']['#markup'] = '<p>' . t('No sites found. Add a site on the <a href="@config">Remote Reports configuration page</a>. </p>',
        array('@config' => '/admin/config/system/remote_reports'));
  }
  else {
    // Pass data through theme function.
    // TODO: Manually refresh available data.
//    $available_modules_last_refreshed = variable_get('available_modules_last_refreshed');
//     if (isset($available_modules_last_refreshed)) {
//       $build['available_modules_refresh']['#markup'] = '<p>' . t('The last time all the available modules where fetched from drupal.org was at: @last_refreshed. <a href="@link">Refresh manually</a>.',
//           array(
//             '@last_refreshed' => date('H:i d/m/Y', $available_modules_last_refreshed),
//             '@link' => '/admin/remote_reports/refresh/available_modules?destination=' . drupal_get_destination()['destination'],
//           )
//         ) . '</p>';
//     }
    $available_modules_last_refreshed = variable_get('available_modules_last_refreshed');
    if (isset($available_modules_last_refreshed)) {
      $build['available_modules_refresh']['#markup'] = '<p>' . t('The last time all the available modules where fetched from drupal.org was at: @last_refreshed.',
          array(
            '@last_refreshed' => date('H:i d/m/Y', $available_modules_last_refreshed),
          )
        ) . '</p>';
    }
    // Render the sites table.
    $build['sites'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No sites found. Add a site on the <a href="@config">Remote Reports configuration page</a>. </p>',
        array('@config' => '/admin/config/system/remote_reports')),
    );
  }
  return $build;
}

/**
 * Title for site details page.
 */
function remote_reports_page_site_details_title($sid) {
  $site = _get_site($sid);
  if ($site) {
    $title = t('Site:') . ' ' . $site->title;
    return $title;
  }
  else {
    return FALSE;
  }
}

/**
 * Site details page.
 */
function remote_reports_page_site_details($sid) {
  $build = array();
  // Get status summary for this site.
  $status = _get_site_status($sid);
  // Check if site is found.
  if ($status) {
    $site_details_rows = array(
      array(
        'data' => array(
          'title' => t('Site name'),
          'value' => l($status->title, '/admin/remote_reports/site/' . $status->sid),
        ),
      ),
      array(
        'data' => array(
          'title' => t('Hostname'),
          'value' => $status->hostname,
        ),
      ),
      array(
        'data' => array(
          'title' => t('Site url'),
          'value' => l($status->url, $status->url),
        ),
      ),
    );
    $site_status_rows = array(
      array(
        'data' => array(
          'title' => t('Errors'),
          'value' => $status->errors,
        ),
        'class' => $status->errors ? array('error') : array('ok'),
      ),
      array(
        'data' => array(
          'title' => t('Warnings'),
          'value' => $status->warnings,
        ),
        'class' => $status->warnings ? array('warning') : array('ok'),
      ),
      array(
        'data' => array(
          'title' => t('Checks'),
          'value' => $status->checks,
        ),
      ),
    );

    $site_module_updates_rows = array(
      array(
        'data' => array(
          'title' => t('Security updates'),
          'value' => $status->not_secure,
        ),
        'class' => $status->not_secure ? array('error') : array('ok'),
      ),
      array(
        'data' => array(
          'title' => t('Not supported'),
          'value' => $status->not_supported,
        ),
        'class' => $status->not_supported ? array('warning') : array('ok'),
      ),
      array(
        'data' => array(
          'title' => t('Update available'),
          'value' => $status->not_current,
        ),
        'class' => $status->not_current ? array('warning') : array('ok'),
      ),
    );
    $site_modules_rows = array();
    // Get installed modules for a module list.
    $modules = _cache_site_modules_get($sid);
    foreach ($modules as $module) {
      $site_modules_rows[] = array(
        'data' => array(
          'title' => $module['name'],
          'value' => $module['info']['version'],
        ),
        'class' => $module['project_status'] ? array('ok') : array('error'),
      );
    }
    // Pass data through theme function.
    $build['site_details_title']['#markup'] = '<h3>' . t('Details') . '</h3>';
    $build['site_details'] = array(
      '#theme' => 'table',
      '#rows' => $site_details_rows,
      '#attributes' => array(
        'class' => array('counter-table'),
      ),
    );
    $build['site_status_title']['#markup'] = '<h3>' . t('Status checks') . '</h3>';
//    $build['site_status_subtitle']['#markup'] = '<p>' .
//      t('The last time the status was refreshed was at: @date. <a href="@refresh">Refresh manually</a>.',
//        array(
//          '@date' => date('H:i d/m/Y', $status->status_created),
//          '@refresh' => '/admin/remote_reports/refresh/status/' . $status->sid . '?destination=' . drupal_get_destination()['destination'],
//        )) . '</p>';
  $build['site_status_subtitle']['#markup'] = '<p>' .
    t('The last time the status was refreshed was at: @date.',
      array(
        '@date' => date('H:i d/m/Y', $status->status_created),
      )) . '</p>';
    $build['site_status'] = array(
      '#theme' => 'table',
      '#rows' => $site_status_rows,
      '#attributes' => array(
        'class' => array('counter-table'),
      ),
    );
    $build['site_module_updates_title']['#markup'] = '<h3>' . t('Module updates') . '</h3>';
//    $build['site_module_updates_subtitle']['#markup'] = '<p>' .
//      t('The last time the available updates and modules were refreshed was at: @date. <a href="@refresh">Refresh manually</a>.',
//        array(
//          '@date' => date('H:i d/m/Y', $status->modules_created),
//          '@refresh' => '/admin/remote_reports/refresh/site_updates/' . $status->sid . '?destination=' . drupal_get_destination()['destination'],
//        )) . '</p>';
    $build['site_module_updates_subtitle']['#markup'] = '<p>' .
      t('The last time the available updates and modules were refreshed was at: @date.',
        array(
          '@date' => date('H:i d/m/Y', $status->modules_created),
        )) . '</p>';
    $build['site_module_updates'] = array(
      '#theme' => 'table',
      '#rows' => $site_module_updates_rows,
      '#attributes' => array(
        'class' => array('counter-table'),
      ),
    );
    $build['site_modules_title']['#markup'] = '<h3>' . t('Installed modules') . '</h3>';
//    $build['site_modules_subtitle']['#markup'] = '<p>' .
//      t('The last time the installed modules were refreshed was at: @date. <a href="@refresh">Refresh manually</a>.',
//        array(
//          '@date' => date('H:i d/m/Y', array_values($modules)[0]['created']),
//          '@refresh' => '/admin/remote_reports/refresh/site_modules/' . $status->sid . '?destination=' . drupal_get_destination()['destination'],
//        )) . '</p>';
    $build['site_modules_subtitle']['#markup'] = '<p>' .
      t('The last time the installed modules were refreshed was at: @date.',
        array(
          '@date' => date('H:i d/m/Y', array_values($modules)[0]['created']),
        )) . '</p>';
    $build['site_modules'] = array(
      '#theme' => 'table',
      '#rows' => $site_modules_rows,
      '#attributes' => array(
        'class' => array('counter-table'),
      ),
    );
  }
  else {
    // If the site isn't found: Give a error and redirect to dashboard.
    drupal_set_message(t("The site with sid: %id couln't be found.", array('%id' => $sid)), 'error');
    drupal_goto('admin/remote_reports');
  }
  return $build;
}

/**
 * Status report page.
 */
function remote_reports_page_status($sid) {
  $build = array();
  // Get site info.
  $site = _get_site($sid);
  // Check if the site is found.
  if ($site) {
    // Get status report from cache.
    $cache = _cache_status_get($sid);
    $data = $cache->data;
    // Pass data through the status_report theme.
    $status_report = theme('status_report', array('requirements' => $data));
    $status_report = str_replace('/admin', $site->url . '/admin', $status_report);
    $status_report = str_replace('?destination=', '#', $status_report);
    // Pass data through theme function.
//    $build['site_status_title']['#markup'] = '<p>' .
//      t('The last time the status was refreshed was at:') . ' ' . date('H:i d/m/Y', $cache->created) . '. ' .
//      l(t('Refresh manually'), '/admin/remote_reports/refresh/status/' . $site->sid, array('query' => drupal_get_destination())) . '</p>';
    $build['site_status_title']['#markup'] = '<p>' .
      t('The last time the status was refreshed was at:') . ' ' . date('H:i d/m/Y', $cache->created) . '</p>';
    $build['status_report']['#markup'] = $status_report;
  }
  else {
    // If the site isn't found: Give a error and redirect to dashboard.
    drupal_set_message(t("The site with sid: %id couln't be found.", array('%id' => $sid)), 'error');
    drupal_goto('admin/remote_reports');
  }
  return $build;
}

/**
 * Update report page.
 */
function remote_reports_page_updates($sid) {
  $build = array();
  // Get site status for the modules_created timestamp.
  $status = _cache_site_status_get($sid);
  // Check if site is found.
  if ($status) {
//    $build['site_modules_subtitle']['#markup'] = '<p>' .
//      t('The last time the modules were refreshed was at:') . ' ' . date('H:i d/m/Y', $status['modules_created']) . '. ' .
//      l(t('Refresh manually'), 'admin/remote_reports/refresh/site_modules/' . $sid, array('query' => drupal_get_destination())) . '</p>';
    $build['site_modules_subtitle']['#markup'] = '<p>' .
      t('The last time the modules were refreshed was at:') . ' ' . date('H:i d/m/Y', $status['modules_created']) . '</p>';
    // Get the module updates.
    $data = _get_module_updates($sid);
    // Pass data through the theme function.
    $updates = theme('update_report', array('data' => $data));
    // Hide the last checked and refresh manually.
    $updates = str_replace('<div class="update checked">', '<div style="display: none;" class="update checked">', $updates);
    $build['updates']['#markup'] = $updates;
  }
  else {
    // If the site isn't found: Give a error and redirect to dashboard.
    drupal_set_message(t("The site with sid: %id couln't be found.", array('%id' => $sid)), 'error');
    drupal_goto('admin/remote_reports');
  }
  return $build;
}
