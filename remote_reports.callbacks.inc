<?php

/**
 * @file
 * Contains Callback pages.
 */

/**
 * Callback to refresh status.
 */
function remote_reports_refresh_status($sid) {
  _refresh_status($sid);
  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
}

/**
 * Callback to refresh available modules.
 */
function remote_reports_refresh_available_modules($sid = NULL) {
  _refresh_available_modules($sid);
  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
}

/**
 * Callback to refresh the installed modules.
 */
function remote_reports_refresh_site_modules($sid) {
  _refresh_site_modules($sid);
  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
}

/**
 * Callback to refresh update information.
 */
function remote_reports_refresh_site_updates($sid) {
  _refresh_site_updates($sid);
  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
}

/**
 * Callback to refresh all site information.
 */
function remote_reports_refresh_site($sid) {
  _refresh_site($sid);
  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
}

/**
 * Callback to return an autocompletion for a module.
 */
function remote_reports_autocomplete_modules($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query("SELECT DISTINCT (name) FROM {cache_remote_reports_site_modules} WHERE name LIKE :name", array(':name' => db_like($string) . '%'))->fetchAll();
    foreach ($result as $module) {
      $matches[$module->name] = check_plain($module->name);
    }
  }
  drupal_json_output($matches);
}
