<?php

/**
 * @file
 * Contains functions.
 */

/**
 * Set status report in cache.
 */
function _cache_status_set($sid, $data, $expire) {
  $fields = array(
    'created' => REQUEST_TIME,
    'expire' => $expire,
  );
  if (!is_string($data)) {
    $fields['data'] = serialize($data);
    $fields['serialized'] = 1;
  }
  else {
    $fields['data'] = $data;
    $fields['serialized'] = 0;
  }
  // Merge in database.
  db_merge('cache_remote_reports_status')
    ->key(array('sid' => $sid))
    ->fields($fields)
    ->execute();
}

/**
 * Get status report from cache.
 */
function _cache_status_get($sid) {
  $cache = db_query("SELECT data, created, expire, serialized FROM {cache_remote_reports_status} WHERE sid = :sid", array(':sid' => $sid))->fetchObject();
  if (isset($cache->data)) {
    $cache->data = $cache->serialized ? unserialize($cache->data) : $cache->data;
  }
  // Return unserialized data.
  return $cache;
}

/**
 * Set modules used by site.
 */
function _cache_site_modules_set($sid, $data, $expire) {
  foreach ($data as $module) {
    $fields = array(
      'created' => REQUEST_TIME,
      'expire' => $expire,
    );
    if (!is_string($module)) {
      $fields['data'] = serialize($module);
      $fields['serialized'] = 1;
    }
    else {
      $fields['data'] = $module;
      $fields['serialized'] = 0;
    }
    // Merge in database.
    db_merge('cache_remote_reports_site_modules')
      ->key(array('sid' => $sid, 'name' => $module['name']))
      ->fields($fields)
      ->execute();
  }
}

/**
 * Get all modules or from a site or get sites that use a module.
 */
function _cache_site_modules_get($sid = NULL, $module = NULL) {
  if (isset($sid)) {
    $cache = db_query("SELECT name, data, created, expire, serialized FROM {cache_remote_reports_site_modules} WHERE sid = :sid", array(':sid' => $sid))->fetchAll();
    // Unserialize each item and put it in an array.
    $data = array();
    foreach ($cache as $item) {
      if (isset($item->data)) {
        $data[$item->name] = $item->serialized ? unserialize($item->data) : $item->data;
        $data[$item->name]['created'] = $item->created;
      }
    }
  }
  else {
    if (isset($module)) {
      $cache = db_query("SELECT sid, name, data, created, expire, serialized FROM {cache_remote_reports_site_modules} WHERE name = :name", array(':name' => $module))->fetchAll();
      // Unserialize each item and put it in an array.
      $data = array();
      foreach ($cache as $key => $item) {
        if (isset($item->data)) {
          $data[$key] = $item->serialized ? unserialize($item->data) : $item->data;
          $data[$key]['sid'] = $item->sid;
          $data[$key]['created'] = $item->created;
          $data[$key]['expire'] = $item->expire;
        }
      }
    }
    else {
      $cache = db_query("SELECT DISTINCT (name), data, created, expire, serialized FROM {cache_remote_reports_site_modules}", array(':sid' => $sid))->fetchAll();
      // Unserialize each item and put it in an array.
      $data = array();
      foreach ($cache as $item) {
        if (isset($item->data)) {
          $data[$item->name] = $item->serialized ? unserialize($item->data) : $item->data;
          $data[$item->name]['created'] = $item->created;
        }
      }
    }
  }
  // Return the unserialized data in an array.
  return $data;
}

/**
 * Set available modules.
 */
function _cache_module_set($data, $expire) {
  $id = $data['short_name'];
  $fields = array(
    'id' => $id,
    'created' => REQUEST_TIME,
    'expire' => $expire,
  );
  if (!is_string($data)) {
    $fields['data'] = serialize($data);
    $fields['serialized'] = 1;
  }
  else {
    $fields['data'] = $data;
    $fields['serialized'] = 0;
  }
  // Merge in database.
  db_merge('cache_remote_reports_modules')
    ->key(array('id' => $id))
    ->fields($fields)
    ->execute();
}

/**
 * Get available modules.
 */
function _cache_module_get($id = NULL) {
  if (isset($id)) {
    $cache = db_query("SELECT data, created, expire, serialized FROM {cache_remote_reports_modules} WHERE id = :id", array(':id' => $id['name']))->fetchObject();
    if (isset($cache->data)) {
      // Unserialize data.
      $data = $cache->serialized ? unserialize($cache->data) : $cache->data;
      $data['created'] = $cache->created;
      // Return the modules of a specific site.
      return $data;
    }
  }
  else {
    $cache = db_query("SELECT id, data, created, expire, serialized FROM {cache_remote_reports_modules}", array(':id' => $id))->fetchAll();
    if (isset($cache)) {
      $data = array();
      foreach ($cache as $item) {
        if (isset($item->data)) {
          // Unserialize data.
          $data[$item->id] = $item->serialized ? unserialize($item->data) : $item->data;
          $data[$item->id]['created'] = $item->created;
        }
      }
      // Return all modules.
      return $data;
    }
  }
  // Return false if nothing was found.
  return FALSE;
}

/**
 * Set report summaries for a site.
 */
function _cache_site_status_set($sid, $status, $modules, $expire) {
  $fields['sid'] = $sid;
  if (isset($status)) {
    $fields['errors'] = $status['errors'];
    $fields['warnings'] = $status['warnings'];
    $fields['checks'] = $status['checks'];
    $fields['status_created'] = REQUEST_TIME;
  }
  if (isset($modules)) {
    $fields['not_secure'] = $modules['not_secure'];
    $fields['not_supported'] = $modules['not_supported'];
    $fields['not_current'] = $modules['not_current'];
    $fields['modules_created'] = REQUEST_TIME;
  }
  $fields['expire'] = $expire;
  // Merge in database.
  db_merge('cache_remote_reports_site_status')
    ->key(array('sid' => $sid))
    ->fields($fields)
    ->execute();
}

/**
 * Get report summaries for a site.
 */
function _cache_site_status_get($sid, $header = NULL) {
  $query = db_select('cache_remote_reports_site_status', 'status')
    ->fields('status')
    ->condition('sid', $sid, '=')
    ->execute()
    ->fetchAssoc();
  // Return array with integers.
  return $query;
}

/**
 * Clear cache tables.
 */
function _cache_clear($sid = NULL, $clear = NULL) {
  if ($sid) {
    // Only delete $clear if set.
    if ($clear == 'status' || is_null($clear)) {
      db_delete('cache_remote_reports_status')
        ->condition('sid', $sid, '=')
        ->execute();
    }
    if ($clear == 'site_modules' || is_null($clear)) {
      db_delete('cache_remote_reports_site_modules')
        ->condition('sid', $sid, '=')
        ->execute();
    }
  }
}

/**
 * Get site from database with id.
 */
function _get_site($sid) {
  // Use Database API to retrieve the site.
  $query = db_select('remote_reports_sites', 'sites')
    ->fields('sites')
    ->condition('sid', $sid, '=')
    ->execute()
    ->fetchObject();

  return $query;
}

/**
 * Get all sites from database.
 */
function _get_sites($header = NULL) {
  // Use Database API to retrieve sites.
  if (isset($header)) {
    $query = db_select('remote_reports_sites', 'sites')
      ->fields('sites')
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute();
  }
  else {
    $query = db_select('remote_reports_sites', 'sites')
      ->fields('sites')
      ->execute();
  }

  return $query;
}

/**
 * Get JSON from site.
 */
function _get_api_request($sid, $path) {
  $site = _get_site($sid);
  $url = $site->url . '/remote_reports_server/' . $path;
  $data = drupal_http_request($url, array('headers' => array('validation_key' => $site->api_key)));
  // Than check if the permissions are set (or if the page is right)
  if ($data->code == 403) {
    drupal_set_message(t('The page for the site %site has the wrong permissions check if <a href="@site_url">the permissions</a> are set correctly.',
      array(
        '@site_url' => $site->url . '/admin/people/permissions#module-remote_reports_server',
        '%site' => $site->title,
      )), 'error');
  }
  // Than check if the site returns an ok reponse.
  elseif ($data->code != 200) {
    drupal_set_message(t('The url %url for the site %site is not valid', array(
      '%url' => $site->url,
      '%site' => $site->title,
    )), 'error');
  }
  // Then check if the API key is valid.
  elseif ($data->data == 'Unauthorized or missing API key') {
    drupal_set_message(t('The API key %api_key for the site %site is not valid', array(
      '%api_key' => $site->api_key,
      '%site' => $site->title,
    )), 'error');
  }
  else {
    return $data;
  }
  return FALSE;
}

/**
 * Get the available module releases.
 */
function _get_available_modules($project) {
  global $base_url;
  module_load_include('inc', 'update', 'update.fetch');
  $max_fetch_attempts = 1;

  $available = array();
  $site_key = drupal_hmac_base64($base_url, drupal_get_private_key());
  $url = _update_build_fetch_url($project, $site_key);
  $fetch_url_base = _update_get_fetch_url_base($project);
  $project_name = $project['name'];

  if (empty($fail[$fetch_url_base]) || $fail[$fetch_url_base] < $max_fetch_attempts) {
    $xml = drupal_http_request($url);
    if (!isset($xml->error) && isset($xml->data)) {
      $data = $xml->data;
    }
  }
  if (!empty($data)) {
    $available = update_parse_xml($data);
    if (!empty($available)) {
      // Only if we fetched and parsed something sane do we return success.
      $success = TRUE;
      // Cache the available module releases.
      _cache_module_set($available, time() + 60 * 60 * 12);
    }
  }
  else {
    $success = FALSE;
  }
  return $success;
}

/**
 * Get report summaries for a site. Including site information.
 */
function _get_site_status($sid = NULL, $header = NULL) {
  $query = db_select('remote_reports_sites', 'sites');
  $query->leftJoin('cache_remote_reports_site_status', 'status', 'sites.sid = status.sid');
  if (isset($sid)) {
    if (isset($header)) {
      $result = $query->fields('sites', array(
        'sid',
        'title',
        'hostname',
        'url',
        'api_key',
      ))
        ->fields('status')
        ->condition('sites.sid', $sid, '=')
        ->extend('TableSort')
        ->orderByHeader($header)
        ->execute()
        ->fetchObject();
    }
    else {
      $result = $query->fields('sites', array(
        'sid',
        'title',
        'hostname',
        'url',
        'api_key',
      ))
        ->fields('status')
        ->condition('sites.sid', $sid, '=')
        ->execute()
        ->fetchObject();
    }
  }
  else {
    $query = db_select('remote_reports_sites', 'sites');
    $query->leftJoin('cache_remote_reports_site_status', 'status', 'sites.sid = status.sid');
    $result = $query->fields('sites', array(
      'sid',
      'title',
      'hostname',
      'url',
      'api_key',
    ))
      ->fields('status')
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute()
      ->fetchAll();
  }
  // Return joint tables as array.
  return $result;
}

/**
 * Get update report and cache summary.
 */
function _get_module_updates($sid, $message = TRUE) {
  $data = array();
  // Get all available module releases.
  $modules = _cache_module_get();
  // Get information for installed modules.
  $site_modules = _cache_site_modules_get($sid);
  // Process the module information.
  update_process_project_info($site_modules);

  $not_secure = 0;
  $not_supported = 0;
  $not_current = 0;
  foreach ($site_modules as $site_module) {
    if (isset($modules[$site_module['name']])) {
      $module = $modules[$site_module['name']];
    }
    else {
      if ($message) {
        drupal_set_message(t("The module %module couldn't be found in cache, try to refresh the available modules.", array('%module' => $site_module['name'])), 'error', TRUE);
      }
    }
    if (isset($module)) {
      // Calculate the updates.
      update_calculate_project_update_status(NULL, $site_module, $module);
      // Place the update information in the data array.
      $data[$site_module['name']] = $site_module;
      // Count updates to make summary.
      switch ($site_module['status']) {
        case UPDATE_NOT_SECURE:
          $not_secure++;
          break;

        case UPDATE_NOT_SUPPORTED:
          $not_supported++;
          break;

        case UPDATE_NOT_CURRENT:
          $not_current++;
          break;

        default:
          break;
      }
    }
  }
  $counters = array(
    'not_secure' => $not_secure,
    'not_supported' => $not_supported,
    'not_current' => $not_current,
  );
  if (!$message) {
    // Cache the summary if page isn't visited.
    _cache_site_status_set($sid, NULL, $counters, time() + 60 * 60 * 12);
  }
  // Return update data.
  return $data;
}

/**
 * Fetch status report from site and cache it.
 */
function _refresh_status($sid) {
  // Clear status table.
  _cache_clear($sid, 'status');
  // Fetch status report from site.
  $data = _get_api_request($sid, 'status');
  if ($data) {
    // Decode fetched JSON.
    $data = json_decode($data->data, TRUE);
    // Count errors and warnings to make summary.
    $errors = 0;
    $warnings = 0;
    foreach ($data as $index => $item) {
      if (isset($item['severity'])) {
        if ($item['severity'] == 2) {
          $errors++;
        }
        elseif ($item['severity'] == 1) {
          $warnings++;
        }
      }
    }
    $counters = array(
      'errors' => $errors,
      'warnings' => $warnings,
      'checks' => count($data),
    );
    // Cache the status report and summary.
    _cache_site_status_set($sid, $counters, NULL, time() + 60 * 60 * 24);
    _cache_status_set($sid, $data, time() + 60 * 60 * 24);
    $success = TRUE;
  }
  else {
    $success = FALSE;
  }
  return $success;
}

/**
 * Fetch all available modules.
 */
function _refresh_available_modules($sid = NULL) {
  // Get all the installed modules.
  $cache = _cache_site_modules_get($sid);
  // Create queue for better fetching.
  $queue = DrupalQueue::get('get_available_modules');
  // Fetch available module releases for this module.
  foreach ($cache as $project) {
    // _get_available_modules($project);
    $module = _cache_module_get($project);
    // Put project in queue if it wasn't refreshed within 5 hours.
    if ($module['created'] < time() - (60 * 60 * 5)) {
      $queue->createItem($project);
    }
  }
  if (!isset($sid)) {
    variable_set('available_modules_last_refreshed', time());
  }
}

/**
 * Fetch module information from site.
 */
function _refresh_site_modules($sid) {
  // Clear the site_modules table.
  _cache_clear($sid, 'site_modules');
  // Fetch the installed modules.
  $data = _get_api_request($sid, 'modules');
  if ($data) {
    // Decode fetched JSON.
    $data = json_decode($data->data, TRUE);
    // Cache the installed modules.
    _cache_site_modules_set($sid, $data, time() + 60 * 60 * 48);
  }
}

/**
 * Fetch available modules and calculate updates.
 */
function _refresh_site_updates($sid) {
  // Also check if the site has new modules.
  _refresh_site_modules($sid);
  // Fetch the available updates (for this site) and check for updates.
  _refresh_available_modules($sid);
  _get_module_updates($sid, FALSE);
}

/**
 * Fetch all data available from site.
 */
function _refresh_site($sid) {
  // If the status doesn't return an error proceed with the module updates.
  if (_refresh_status($sid)) {
    _refresh_site_updates($sid);
  }
}
